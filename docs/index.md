# Frame Library

## Introduction

A frame is a unit of information containing all the information necessary for the understanding of the interferometer behavior over a finite time interval which integrates several samplings. It contains thus not only the sampling performed during the integrated time interval, but also those performed at a frequency smaller than the frame frequency.

To simplify its manipulation, a frame is organized as a set of C structures described by a header holding pointers to additional structures and values of parameters expected to be stable over the integrated time interval: the starting time of the frame, its duration, values produced by the slow monitoring. This header is followed by an arbitrary number of additional structures, each holding the values of a rapidly varying parameter like the main signal, the seismic noise, etc...

This frame structure is a standard which has to be conserved over the various stages of the analysis. Thus Frame history, detector geometry, trigger results, monitoring data, reconstructed data, simulation results just lead to additional structures. It is always possible to add new structures or to drop old ones.This standard format is the one used by the LIGO and VIRGO Gravitational Wave Detectors.

This Document described the software used to manipulate the frames. The definition of the various structures as well as their representation on tape is described in specification document.

## Links

* Latest version: [http://software.igwn.org/sources/source/framel-8.39.2.tar.xz](http://software.igwn.org/sources/source/framel-8.39.2.tar.xz)